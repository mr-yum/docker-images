# This is used by the ci server to deploy

Build:

```shell
IMAGE="mryum/aws-kubectl"
VERSION="$(cat ./VERSION)"
MINOR_VERSION="${VERSION%.*}"
MAJOR_VERSION="${MINOR_VERSION%.*}"
COMMIT_HASH="$(git rev-parse --short HEAD)"
docker build \
	-t "$IMAGE:$COMMIT_HASH" \
	-t "$IMAGE" \
	-t "$IMAGE:$VERSION" \
	-t "$IMAGE:$MINOR_VERSION" \
	-t "$IMAGE:$MAJOR_VERSION" \
	-t "$IMAGE:$(date -u +%Y%m%dT%H%M)" \
	.
```

Push:

```shell
docker push -a mryum/aws-kubectl
```
