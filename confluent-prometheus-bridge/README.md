# confluent-prometheus-bridge

This docker image is a copy of `ccloudexporter` repo.

We don't use the public image for 2 reasons:

 - It has no versioning (`latest` only)
 - For security/compliance reasons (as we cannot verify integrity of an external image well)

## Quickstart

Init submodule:

```
git submodule update --init --recursive
```

Increment version:

```
printf "0.0.2" > VERSION
```

Build image:

```shell
pushd ccloudexporter
VERSION="$(cat ../VERSION)"
MINOR_VERSION="${VERSION%.*}"
MAJOR_VERSION="${MINOR_VERSION%.*}"
COMMIT_HASH="$(git rev-parse --short HEAD)"
docker build \
	-t "mryum/confluent-prometheus-bridge:ccloudexporter-$COMMIT_HASH" \
	-t "mryum/confluent-prometheus-bridge" \
	-t "mryum/confluent-prometheus-bridge:$VERSION" \
	-t "mryum/confluent-prometheus-bridge:$MINOR_VERSION" \
	-t "mryum/confluent-prometheus-bridge:$MAJOR_VERSION" \
	-t "mryum/confluent-prometheus-bridge:$(date -u +%Y%m%dT%H%M)" \
	.
popd
```

Push image:

```
docker image push --all-tags mryum/confluent-prometheus-bridge
```

## Upgrading submodules

```
git submodule update --remote ccloudexporter
```

## Running the image

Download `ccloud`: https://docs.confluent.io/ccloud-cli/current/install.html


Login and get the token:
```
ccloud login
ccloud api-key create --resource cloud
```

Run the image:
```
$ docker run \
  -e CCLOUD_API_KEY=%API_KEY_HERE% \
  -e CCLOUD_API_SECRET=%API_SECRET_HERE% \
  -e CCLOUD_CLUSTER=%CLUSTER_ID_HERE% \
  -p 2112:2112 \
  mryum/confluent-prometheus-bridge:latest

{
  "level": "info",
  "msg": "Listening on http://:2112/metrics\n",
  "time": "2021-02-24T04:37:35Z"
}
```

# TODO:

- Automate image build / push.