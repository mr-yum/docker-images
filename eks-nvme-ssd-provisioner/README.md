# MrYum notes

Some notes on our use of this project:
- This is essentially copied from https://github.com/brunsgaard/eks-nvme-ssd-provisioner
- Used in the mryum/eks-nvme-provisioner Helm chart
- Used to provision local SSD storage for CockroachDB nodes
- Hosted on DockerHub as: mryum/eks-nvme-ssd-provisioner:v1.0.0

TODO(artem): Coalesce our deployment with the upstream project.

# eks-nvme-ssd-provisioner

The eks-nvme-ssd-provisioner will format and mount NVMe SSD disks on EKS
nodes. This is needed to make the sig-storage-local-static-provisioner work
well with EKS clusters. The eks-nvme-ssd-provisioner will create a raid0 device
if multiple NVMe SSD disks are found.

IMPORTANT NOTE: by default the provisioner will NOT format your disks and will only link block devices to the folder /pv-disks to be used later by sig-storage-local-static-provisioner.
 If you'd like to format your disks and create a filesystem make sure you change eks-nvme-ssd-provisioner.yaml command section as follows:
```
        command:
          - "/bin/bash"
          - "eks-nvme-ssd-provisioner.sh block"
```

The resources in `manifests` expect the following node selector

```
aws.amazon.com/eks-local-ssd: "true"
```

Therefore you must make sure to set that label on all nodes that you want to
use with the eks-nvme-ssd-provisioner and sig-storage-local-static-provisioner.

## Install

Install the DaemonSet by applying the following resource
```
kubectl apply -f manifests/eks-nvme-ssd-provisioner.yaml
```

Optionally you can also apply a pre-configed local-storage-provisioner that
plays well with the eks-nvme-ssd-provisioner
```
kubectl apply -f manifests/storage-local-static-provisioner.yaml
```

## Relation to sig-storage-local-static-provisioner
 - eks-nvme-ssd-provisioner creates disks from block storage
 - sig-storage-local-static-provisioner creates PersistentVolumens from disks 

In most cases you want both, if you have a another way to setup you disks jump directly to
[sig-storage-local-static-provisioner](https://github.com/kubernetes-sigs/sig-storage-local-static-provisioner)
