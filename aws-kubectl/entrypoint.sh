#!/bin/sh
set -ex

aws --region ${CLUSTER_REGION} eks update-kubeconfig --name ${ISTIO_CLUSTER_NAME} --alias ${ISTIO_CLUSTER_NAME}

helm repo add mryum https://gitlab.com/mr-yum/charts/-/raw/master/

exec "$@"
